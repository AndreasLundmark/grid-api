﻿using UnityEngine;
using System.Collections;

public class ExampleFile: MonoBehaviour {

    CreateGrid createGrid = new CreateGrid();
    public GameObject tilePrefab = null;
    public string gridName = "Grid";
    public Material selectMaterial = null;
    public int gridSize = 10;
    public float gridPadding = 0f;

    public GameObject[] decorations;
    public int maximumDecorationsPerTile = 3;
    public int minimumDecorationsPerTile = 0;

    // Use this for initialization
    void Start () {
        createGrid.SetPrefab(tilePrefab);
        createGrid.SetTilePadding(gridPadding);
        createGrid.SetGridSize(gridSize);
        createGrid.SetTileMaterial(selectMaterial);
        createGrid.SetGridName(gridName);
        createGrid.decorationListInput(decorations);
        createGrid.SetMinMaxDecorationPerTile(minimumDecorationsPerTile, maximumDecorationsPerTile);
        createGrid.CreateGridRun();
        createGrid.DecorationPlacement();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
