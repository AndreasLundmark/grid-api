﻿using UnityEngine;
using System.Collections;

public class CreateGrid {

    private GameObject selectedPrefab = null;
    private GameObject tempPrefab = null;
    private GameObject parentObject = null;

    private GameObject decorationParentObject = null;
    private GameObject[] decorationList;
    private GameObject tempDecoration = null;
    private int maximumDecorationsPerTile = 3;
    private int minimumDecorationsPerTile = 0;
    private float marginForDEcorationsOnTile = 3;


    private Vector3 gridDestinationPointInScene = new Vector3(0, 0, 0);
    private string gridName = ("Grid");

    private float tileSizeInUnits = 1.0f;
    private int gridSizeInUnits = 10;
    private float gridTilePadding = 0.0f;

    private Material gridMaterial = null;
    private Texture gridTexure = null;

    public void CreateGridRun()
    {
        parentObject = new GameObject(gridName);

        if (selectedPrefab != null)
        {
            for (int i = 0; i < gridSizeInUnits; i++)
            {
                for (int j = 0; j < gridSizeInUnits; j++)
                {
                    tempPrefab = GameObject.Instantiate(selectedPrefab, new Vector3(i +(i*gridTilePadding), 0, j+(j * gridTilePadding)), Quaternion.identity) as GameObject;
                    if(gridMaterial != null)
                        tempPrefab.transform.GetComponent<Renderer>().material = gridMaterial;
                    //if(gridTexure != null)
                    //    tempPrefab.transform.GetComponent<Renderer>().material.SetTexture = gridTexure;
                    //tempPrefab.transform.localScale = new Vector3(tileSizeInUnits, 0, tileSizeInUnits);
                    tempPrefab.gameObject.transform.SetParent(parentObject.transform);
                }
            }
        }

        parentObject.transform.position = gridDestinationPointInScene;
    }

    public void DecorationPlacement()
    {
        decorationParentObject = new GameObject("decorations");

        for (int i = 0; i < gridSizeInUnits; i++)
        {
            for (int j = 0; j < gridSizeInUnits; j++)
            {
              int randomNumberOfObejcts = Random.Range(minimumDecorationsPerTile, maximumDecorationsPerTile);
                for (int v = 0; v < randomNumberOfObejcts; v++)
                {
                    int randomNumber = Random.Range(0, decorationList.Length);
                    GameObject randomDecoration = decorationList[randomNumber];
                    float positionOnTile = Random.Range((-selectedPrefab.GetComponent<Renderer>().bounds.size.x / marginForDEcorationsOnTile), (selectedPrefab.GetComponent<Renderer>().bounds.size.x / marginForDEcorationsOnTile));
                    
                    if (randomNumberOfObejcts != 0)
                    {
                            tempDecoration = GameObject.Instantiate(randomDecoration, new Vector3(i + (i * gridTilePadding) + positionOnTile, 0, j + (j * gridTilePadding) + positionOnTile), Quaternion.identity) as GameObject;
                            //CheckAgainstChildrenInParent(tempDecoration, decorationParentObject);
                            tempDecoration.gameObject.transform.SetParent(decorationParentObject.transform);
                    }
                }
            }
        }
        decorationParentObject.gameObject.transform.SetParent(parentObject.transform);
    }

    //private void CheckAgainstChildrenInParent(GameObject checkObject,GameObject parent)
    //{
    //    for(int i=0; i < 4; i++)
    //    {

    //    }
    //}

    public void SetPrefab(GameObject prefab)
    {
        selectedPrefab = prefab;
    }

    public void SetGridSize(int size)
    {
        gridSizeInUnits = size;
    }

    public void SetTileSize(float size)
    {
        tileSizeInUnits = size;
    }

    public void SetTilePadding(float padding)
    {
        gridTilePadding = padding;
    }

    public void SetTileTexture(int size) // inte färdig gör om
    {

    }

    public void SetTileMaterial(Material selectedMaterial)
    {
        gridMaterial = selectedMaterial;
    }

    public void SetGridName(string name)
    {
        gridName = name;
    }

    public void decorationListInput(GameObject[] list)
    {
        decorationList = list;
    }

    public void SetMinMaxDecorationPerTile(int min, int max)
    {
         maximumDecorationsPerTile = max;
         minimumDecorationsPerTile = min;
    }

    public void GetTilePositions(int size) // inte färdig gör om
    {

    }

    public GameObject GetGridGameObject()
    {
        return parentObject;
    }

    public GameObject GetDecorationsParentGameObject()
    {
        return decorationParentObject;
    }

    public GameObject GetTileGameObject()
    {
        return selectedPrefab;
    }

    public int GetGridSizeValue()
    {
        return gridSizeInUnits;
    }

    public float GetGridPaddingValue()
    {
        return gridTilePadding;
    }

    public Material GetGridTileMaterial()
    {
        return gridMaterial;
    }
}
